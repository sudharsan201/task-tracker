import os
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# For Production Purpose
# DEBUG = False


# DATABASE CONFIGURATION FOR LOCAL

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "task_tracker",
        "USER": "tracker",
        "PASSWORD": "task@123",
        "HOST": "localhost",
        "PORT": "5432",
    }
}

INTERNAL_IPS = ["127.0.0.1"]
