from django.shortcuts import get_object_or_404
from rest_framework import response, decorators, permissions, status
from .serializers import *
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from rest_framework import filters
# from rest_framework.parsers import MultiPartParser, JSONParser
# from django.template.loader import get_template
# from django.http import HttpResponse
# from xhtml2pdf import pisa
# from wsgiref.util import FileWrapper



class CustomPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data, status):
        return response.Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            "limit": self.page_size,
            'current_page': self.page.number,
            'total_pages': self.page.paginator.num_pages,
            'status_code': status,
            'results': data
        })


class UserApi(APIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['username']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(User.objects.all(), pk=pk)
                user = UserSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": user.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = User.objects.filter(
                    Q(role__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = UserSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = User.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = UserSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)


class RoleApi(APIView):
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['username']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Role.objects.all(), pk=pk)
                role = RoleSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": role.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Role.objects.filter(
                    Q(role__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = RoleSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Role.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = RoleSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Role.objects.create(
                    role=data.get('role'),
                    can_view=data.get('can_view'),
                    can_create=data.get('can_create'),
                    can_update=data.get('can_update'),
                    can_delete=data.get('can_delete'),
                )
                serializer = RoleSerializer(queryset)
                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Role Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": status.HTTP_400_BAD_REQUEST,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class RoleDetailsApi(APIView):
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Role.objects.all(), pk=pk)
                role = RoleSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": role.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Role.objects.filter(
                    Q(role__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = RoleSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Role.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = RoleSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Role.objects.get(id=pk)
            queryset.role = data.get('role')
            queryset.can_view = data.get('can_view')
            queryset.can_create = data.get('can_create')
            queryset.can_update = data.get('can_update')
            queryset.can_delete = data.get('can_delete')
            queryset.save()
            serializer = RoleSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Role.objects.get(id=pk)
            queryset.delete()
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class EmployeeApi(APIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['description']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Employee.objects.all(), pk=pk)
                emp = EmployeeSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Employee.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = EmployeeSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Employee.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = EmployeeSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Employee.objects.create(
                    erp_id=data.get('erp_id'),
                    mobile_number=data.get('mobile_number'),
                    employee_name_id=data.get('employee_name_id'),
                    role_type_id=data.get('role_type_id'),
                )
                serializer = EmployeeSerializer(queryset)
                print("serializer", serializer)

                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " User Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class EmployeeDetailsApi(APIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Employee.objects.all(), pk=pk)
                emp = EmployeeSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": emp.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Employee.objects.filter(
                    Q(emp__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = EmployeeSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Employee.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = EmployeeSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Employee.objects.get(id=pk)
            queryset.erp_id = data.get('erp_id')
            queryset.mobile_number = data.get('mobile_number')
            queryset.employee_name_id = data.get('employee_name_id')
            queryset.role_type_id = data.get('role_type_id')
            queryset.save()
            serializer = EmployeeSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Employee.objects.get(id=pk)
            queryset.delete()
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class WebsiteApi(APIView):
    queryset = Website.objects.all()
    serializer_class = WebsiteSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['websitename']

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Website.objects.all(), pk=pk)
                web = WebsiteSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": web.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Website.objects.filter(
                    Q(web__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = WebsiteSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Website.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = WebsiteSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Website.objects.create(
                    websitename=data.get('websitename'),

                )
                serializer = WebsiteSerializer(queryset)
                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Website Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)
            else:
                result = {
                    "status_code": 400,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class WebsiteDetailsApi(APIView):
    queryset = Website.objects.all()
    serializer_class = WebsiteSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Website.objects.all(), pk=pk)
                web = WebsiteSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": web.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Website.objects.filter(
                    Q(web__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = WebsiteSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Website.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = WebsiteSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Website.objects.get(id=pk)
            queryset.websitename = data.get('websitename')

            queryset.save()
            serializer = WebsiteSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Website.objects.get(id=pk)
            queryset.delete()
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class TaskApi(APIView):
    queryset = Tasks.objects.all()
    serializer_class = TaskSerializer
    pagination_class = CustomPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['description']



    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Tasks.objects.all(), pk=pk)
                task = TaskSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": task.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Tasks.objects.filter(
                    Q(task__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = TaskSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Tasks.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = TaskSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            data = request.data
            print(data)
            if data:
                queryset = Tasks.objects.create(
                    description=data.get('description'),
                    reporting_date=data.get('reporting_date'),
                    expected_closure_date=data.get('expected_closure_date'),
                    task_type=data.get('task_type'),
                    assigned_to_id=data.get('assigned_to'),
                    status=data.get('status'),
                    file=data.get('file'),
                )
                if data.get('website'):
                    print(data.get('website'))
                    for obj in data.get('website'):
                        print('obj', obj)
                        website = Website.objects.get(
                            id=obj
                        )
                        queryset.website.add(website)
                        queryset.save()
                        print('queryset', queryset)
                serializer = TaskSerializer(queryset)
                result = {
                    "status_code": status.HTTP_201_CREATED,
                    "message": " Task Successfully Created",
                    "result": serializer.data
                }
                return response.Response(result, status.HTTP_201_CREATED)

            else:
                result = {
                    "status_code": status.HTTP_400_BAD_REQUEST,
                    "message": "Please Enter Data Correctly..."
                }
                return response.Response(result, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class TaskDetailsApi(APIView):
    queryset = Tasks.objects.all()
    serializer_class = TaskSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        result = {}
        paginator = CustomPagination()
        try:
            if kwargs.get('pk'):
                pk = kwargs.get('pk')
                obj = get_object_or_404(Tasks.objects.all(), pk=pk)
                task = TaskSerializer(obj)
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "result": task.data
                }
                return response.Response(result, status.HTTP_200_OK)
            if request.query_params.get('search'):
                queryset = Tasks.objects.filter(
                    Q(task__icontains=request.query_params.get('search')),
                )
                page = paginator.paginate_queryset(queryset, request)
                serializer = TaskSerializer(page, many=True).data
                return paginator.get_paginated_response(serializer, status.HTTP_200_OK)

            queryset = Tasks.objects.all()
            page = paginator.paginate_queryset(queryset, request)
            serializer = TaskSerializer(page, many=True).data
            return paginator.get_paginated_response(serializer, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        try:
            data = request.data
            queryset = Tasks.objects.get(id=pk)
            queryset.website_id = data.get('website_id')
            queryset.description = data.get('description')
            queryset.reporting_date = data.get('reporting_date')
            queryset.expected_closure_date = data.get('expected_closure_date')
            queryset.task_type = data.get('task_type')
            queryset.assigned_to_id = data.get('assigned_to')
            queryset.status = data.get('status')
            queryset.file = data.get('file')
            queryset.save()
            serializer = TaskSerializer(queryset)
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Updated...",
                "result": serializer.data
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            queryset = Tasks.objects.get(id=pk)
            queryset.delete()
            result = {
                "status_code": status.HTTP_200_OK,
                "message": "Successfully Deleted...",
            }
            return response.Response(result, status.HTTP_200_OK)
        except Exception as e:
            result = {
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_400_BAD_REQUEST)


class RoleAccess(APIView):

    def get(self, request, *args, **kwargs):
        result = {}
        try:
            userId = request.query_params.get('user_id')
            queryset = Employee.objects.get(employee_name=userId)
            if queryset:
                result = {
                    "status_code": status.HTTP_200_OK,
                    "message": "Successfully Fetched...",
                    "role": queryset.role_type.role
                }
                return response.Response(result, status.HTTP_200_OK)
            else:
                result = {
                    "status_code": status.HTTP_401_UNAUTHORIZED,
                    "message": "User not registered in Employee. For Queries please,Contact respective Authority",
                }
                return response.Response(result, status.HTTP_401_UNAUTHORIZED)

        except Exception as e:
            result = {
                "status_code": status.HTTP_404_NOT_FOUND,
                "message": str(e),
                "result": []
            }
            return response.Response(result, status.HTTP_404_NOT_FOUND)


class UserRoleList(APIView):
    def get(self, request, *args, **kwargs):
        result = {}
        userList = []
        employeeList = []
        userCount = User.objects.all().count()
        userQueryset = User.objects.all()
        for i in range(userCount):

            try:
                employee_list = Employee.objects.get(employee_name=userQueryset[i].id)
            except Employee.DoesNotExist:
                user_list = User.objects.get(id=userQueryset[i].id)
                userList.append(user_list)
        serializer = UserSerializer(userList, many=True)
        count = len(userList)
        result = {
            "status_code": status.HTTP_200_OK,
            "message": "Successfully Fetched...",
            "count": count,
            "userList": serializer.data
        }
        return response.Response(result, status.HTTP_200_OK)








