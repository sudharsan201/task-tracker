from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
import random

from django.utils.crypto import get_random_string


class Role(models.Model):
    role = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        unique=True,
    )
    can_view = models.BooleanField(
        default=False,
    )
    can_create = models.BooleanField(
        default=False,
    )
    can_update = models.BooleanField(
        default=False,
    )
    can_delete = models.BooleanField(
        default=False,
    )

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.role


class Employee(models.Model):
    erp_id = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        unique=True,
    )
    mobile_number = models.BigIntegerField(
        null=True,
        blank=True
    )
    employee_name = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        unique=True,
    )
    role_type = models.ForeignKey(
        Role,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.employee_name.username

    def create_new_ref_number():
        not_unique = True
        while not_unique:
            unique_ref = random.randint(1000000000, 9999999999)
            if not Employee.objects.filter(Referrence_Number=unique_ref):
                not_unique = False
        return str(unique_ref)



    def save(self, *args, **kwargs):
        self.Reference_Number = get_random_string(10).lower()
        super(Employee, self).save(*args, **kwargs)


class Website(models.Model):
    websitename = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        unique=True,
    )

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.websitename


# Task Data
class Tasks(models.Model):
    STATUS_CHOICES = (
        ('Hold', 'Hold'),
        ('In Progress', 'In Progress'),
        ('Completed', 'Completed'),
    )
    website = models.ManyToManyField(
        Website,
        # related_name="assigned_to",
        null=True,
        blank=True
    )
    description = models.TextField(
        null=True,
        blank=True
    )
    reporting_date = models.DateField()
    expected_closure_date = models.DateField()
    task_type = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )
    assigned_to = models.ForeignKey(
        Employee,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    status = models.CharField(
        choices=STATUS_CHOICES,
        max_length=100,
    )
    file = models.FileField(
        upload_to='uploads/',
    )
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.task_type
