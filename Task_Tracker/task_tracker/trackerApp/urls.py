from django.urls import path, include

from . import views

urlpatterns = [
    path('role/', views.RoleApi.as_view()),
    path('role/<int:pk>', views.RoleDetailsApi.as_view()),
    path('employee/', views.EmployeeApi.as_view()),
    path('employee/<int:pk>', views.EmployeeDetailsApi.as_view()),
    path('task/', views.TaskApi.as_view()),
    path('task/<int:pk>', views.TaskDetailsApi.as_view()),
    path('website/', views.WebsiteApi.as_view()),
    path('website/<int:pk>', views.WebsiteDetailsApi.as_view()),
    path('user/', views.UserApi.as_view()),
    path('role-access/', views.RoleAccess.as_view()),
    path('user-list/',views.UserRoleList.as_view()),

]
