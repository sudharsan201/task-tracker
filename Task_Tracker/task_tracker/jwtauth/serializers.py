# jwtauth/serializers.py

from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import *

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, style={
        "input_type": "password"})
    password2 = serializers.CharField(
        style={"input_type": "password"}, write_only=True, label="Confirm password")

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "email",
            "username",
            "password",
            "password2",
        ]
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        first_name = validated_data["first_name"]
        last_name = validated_data["last_name"]
        email = validated_data["email"]
        username = validated_data["username"]
        password = validated_data["password"]
        password2 = validated_data["password2"]
        if (email  and User.objects.filter( email=email).exclude(
                username=username).exists()):
            raise serializers.ValidationError(
                {"email_id": "Email addresses must be unique."},
               )

        if password != password2:
            raise serializers.ValidationError(
                {"password": "The two passwords differ."})

        user = User(username=username, email=email)
        user.set_password(password)
        user.save()
        return user
