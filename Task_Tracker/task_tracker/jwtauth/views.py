# jwtauth/views.py

from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework import response, decorators, permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from .serializers import UserCreateSerializer, UserSerializer
from django.contrib.auth import authenticate

User = get_user_model()


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def registration(request):
    serializer = UserCreateSerializer(data=request.data)
    if not serializer.is_valid():
        return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
    user = serializer.save()
    refresh = RefreshToken.for_user(user)
    res = {
        "status_code": status.HTTP_201_CREATED,
        "message": "Registered Successfully...",
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }
    return response.Response(res, status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
def login(request):
    if request.method == 'GET':
        blogs = User.objects.all()
        serializer = UserSerializer(blogs, many=True)
        return Response({
            'success': True,
            'message': 'Get request fulfilled!! ',
            'data': serializer.data
        })

    if request.method == 'POST':
        try:
            blogs = User.objects.get(username=request.data['username'])
            user = authenticate(username=request.data['username'], password=request.data['password'])
            print('user:', user)
            if user:
                queryset = User.objects.get(username=user)
                serializer = UserSerializer(queryset)
                return Response({
                    'status_code': status.HTTP_200_OK,
                    'success': True,
                    'message': 'Login Successfully!!',
                    'data': serializer.data,
                })
            else:
                return Response({
                    'status_code': status.HTTP_400_BAD_REQUEST,
                    'success': False,
                    'message': 'Please Enter correct username and password!!',

                })
        except User.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'success': False,
                'message': 'Bad Credentials',
                'data': ''
            })
