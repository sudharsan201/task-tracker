import React , {useEffect } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import './App.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import AdminNavbar from './components/navbars/admin-navbar';
import UserNavBar from './components/navbars/user-navbar';
import Users from './components/users/user-page';
import Tasks from './components/tasks/task-page';
import Website from './components/website/website-page';
import LoginPage from './components/authorization/login-page';
import RegistrationPage  from './components/authorization/registration-page';
import Unauthorized401 from './components/http_responses/401_page';
import BadRequest404 from './components/http_responses/404_page';



const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ff6b6b',
    },
    secondary: {
      main: '#014b7e',
    },
    text: {
      default: '#fff',
    },
   
   
  },
  typography: {
    fontSize: 16,
    color: '#014b7e',
  },

  overrides: {
    MuiButton: {
      // Name of the rule
      root: {
        // Some CSS
        color: '#ffffff',
        backgroundColor: ' #ff6b6b',
        fontFamily:'Nunito',
      },
    },
  },
});
function App() {
const Role = localStorage.getItem('Role');
console.log("Roles",Role);

  return (
    <div className="App">
      <Router>
        <ThemeProvider theme={theme}>
          <Switch>
            <Route exact path='/'>
              {({match, history}) => <LoginPage match={match} history={history}/>}
            </Route>
            <Route exact path='/register'>
              {({match}) => <RegistrationPage match={match}/>}
            </Route>
            {Role && Role === 'Admin' ?
            <Route exact path='/users'>
              {({match}) => <Users match={match}/>}
            </Route>:<Route exact path=''>
             {({match}) => <BadRequest404 match={match}/>}
           </Route>
           }
           {Role && (Role === 'Admin' || Role === 'User') ?
            <Route exact path='/tasks'>
              {({match}) => <Tasks match={match}/>}
            </Route>
            :<Route exact path=''>
            {({match}) => <BadRequest404 match={match}/>}
          </Route>
          }
          {Role && Role === 'Admin' ?
            <Route exact path='/website'>
              {({match}) => <Website match={match}/>}
            </Route>
            :<Route exact path=''>
            {({match}) => <BadRequest404 match={match}/>}
          </Route>
          }
            {Role && Role === 'Admin'?
            <Route exact path='/admin'>
              {({match}) => <AdminNavbar match={match}/>}
            </Route>
            :<Route exact path=''>
            {({match}) => <BadRequest404 match={match}/>}
          </Route>
          }
            {/* {Role && Role === 'User'?
            <Route exact path='/user'>
              {({match}) => <UserNavBar match={match}/>}
            </Route>
            :<Route exact path=''>
            {({match}) => <BadRequest404 match={match}/>}
          </Route>
          } */}
            <Route exact path='/user'>
              {({match}) => <UserNavBar match={match}/>}
            </Route>
          
            <Route exact path='/unauthorized'>
              {({match}) => <Unauthorized401 match={match}/>}
            </Route>
          </Switch>
        </ThemeProvider>

      </Router>
    
    </div>
  );
}

export default App;
