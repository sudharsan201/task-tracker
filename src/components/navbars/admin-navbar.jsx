import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withRouter } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import AssignmentTurnedInOutlinedIcon from '@material-ui/icons/AssignmentTurnedInOutlined';
import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined';
// import Bg from '../../images/TastTrackerBack.png';


import Users from '../users/user-page';
import Task from "../tasks/task-page";
import Website from "../website/website-page";
import Role from '../roles/role-page';

const drawerWidth = 220;


const styles = theme => ({
  root: {
    display: "flex",
  
    
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  menuButtonIconClosed: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(0deg)"
  },
  menuButtonIconOpen: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(180deg)"
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9 + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing.unit,
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  },
  grow: {
    flexGrow: 1
  }
});

class AdminNavbar extends React.Component {
  state = {
    open: false,
    anchorEl: null,
    user:false,
    task:false,
    website:false,
    home:false,
    role:false,
  };


  handleDrawerOpen = () => {
    this.setState({ open: !this.state.open });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
   
  };
  handleClose = () => {
    this.setState({ anchorEl: null });

  };
  handleUser = () => {
    this.setState({ user: true });
    this.setState({task:false});
    this.setState({website:false});  <>
    <Typography paragraph variant='h4'>
      Admin Page
    </Typography>
    <Typography paragraph>
    This is  Task tracker System
    </Typography>
    </>
    this.setState({home:false});
    this.setState({role:false});

  };
  handleTasks = () => {
    this.setState({ task: true });
    this.setState({user:false});
    this.setState({website:false});
    this.setState({home:false});
    this.setState({role:false});

  };
  handleWebsites = () => {
    this.setState({ website: true });
    this.setState({task:false});
    this.setState({user:false});
    this.setState({home:false});
    this.setState({role:false});


  };
  handleHome = () => {
    this.setState({ home: true });
    this.setState({task:false});
    this.setState({user:false});
    this.setState({website:false});
    this.setState({role:false});


  };
  handleRole = () => {
    this.setState({ role: true });
    this.setState({task:false});
    this.setState({user:false});
    this.setState({website:false});
    this.setState({home:false});


  };
  handleLogin = (param) => {
    localStorage.removeItem('User');
    localStorage.removeItem('Role');
    this.props.history.push('/', 1000);
  }


  render() {
    const { match, location, history } = this.props;
    const { classes, theme } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

   

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classes.appBar}
          fooJon={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open
          })}
        >
          <Toolbar disableGutters={true}>
            <IconButton
             
              style={{color:"#fff",fontWeight:"bold"}}
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classes.menuButton}
            >
              <MenuIcon
                classes={{
                  root: this.state.open
                    ? classes.menuButtonIconOpen
                    : classes.menuButtonIconClosed
                }}
              />
            </IconButton>
            <Typography
              variant="h6"
              style={{color:"#fff",fontWeight:"bold"}}
              className={classes.grow}
              noWrap
            >
              Task Tracker
            </Typography>
            <div>
              <IconButton
                aria-owns={open ? "menu-appbar" : undefined}
                aria-haspopup="true"
                style={{color:"#fff",fontWeight:"bold"}}
                onClick={this.handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                style={{color:"#fff",fontWeight:"bold"}}
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                open={open}
                onClose={this.handleClose}
              >
                {/* <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                <MenuItem onClick={this.handleClose}>My account</MenuItem> */}
                <MenuItem onClick={this.handleLogin }>Logout</MenuItem>

              </Menu>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open
            })
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar} />
          <List>
          <ListItem
               button
               onClick={this.handleHome}
               >
                <ListItemIcon>
                    <HomeOutlinedIcon/> 
                </ListItemIcon>
                Home
                <ListItemText />
              </ListItem>
              <ListItem
               button
               onClick={this.handleUser}
               >
                <ListItemIcon>
                    <PersonOutlineOutlinedIcon/> 
                </ListItemIcon>
                Users
                <ListItemText />
              </ListItem>
              <ListItem
               button
               onClick={this.handleRole}
               >
                <ListItemIcon>
                    <SupervisorAccountOutlinedIcon/>
                </ListItemIcon>
                Role
                <ListItemText />
              </ListItem>
              <ListItem
               button
               onClick={this.handleWebsites}
               >
                <ListItemIcon>
                    <LanguageOutlinedIcon/> 
                </ListItemIcon>
                Websites
                <ListItemText />
              </ListItem>
              <ListItem 
              button
              onClick={this.handleTasks}
              >
                <ListItemIcon>
                    <AssignmentTurnedInOutlinedIcon/> 
                </ListItemIcon>
                Tasks
                <ListItemText />
              </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          { !(this.state.user || this.state.task || this.state.website || this.state.role)?
          <>
          <Typography paragraph variant='h4'>
            Admin Page
          </Typography>
          <Typography paragraph>
          This is Task Tracker System
          </Typography>
          </>:
          <>
          {this.state.home?
            <>
            <Typography paragraph variant='h4'>
              Admin Page
            </Typography>
            <Typography paragraph>
            This is  Task tracker System
            </Typography>
            </>:''
          }
          {this.state.user?
            <Users/>:''
          }
          {this.state.task?
            <Task/>:''
          }
          {this.state.website?
            <Website/>:''
          }
          {this.state.role?
            <Role/>:''
          }
          </>
          }
        </main>
      </div>
    );
  }
}

AdminNavbar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

const AdminNavbars = withRouter(AdminNavbar);

export default withStyles(styles, { withTheme: true })(AdminNavbars);
