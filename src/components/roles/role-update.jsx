import Styles from './style.css';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { TextField } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  


});
const useStyles = makeStyles((theme) => ({
    root: {
       
       backgroundRepeat:'no-repeat',
       position:'relative',
       backgroundPosition: "unset",
       backgroundPositionX: "center",
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 300,
      },
  
    },
    logo:{
      marginTop: '6%',
    },
    TableHead:{
      top: 0,
      left: 0,
      zIndex: 2,
      Position: 'sticky',
      BackgroundColor: 'black',
    
     
    },
    name:{
      
      marginBottom: '12px',
      marginTop: '15px',
    },
    label:{
      width: '480px !important',
    },
    paper: {
      
      textAlign: 'center',
      color: theme.palette.text.secondary,
      width:'auto',
      height:'auto',
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'center',
  
    },
    margin: {
      margin: theme.spacing(1),
    },
    
  }));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function RoleForm(props) {

  const { addorEdit, recordForEdit } = props

  const [open, setOpen] = React.useState(false);

  const [role, setRole] = useState([]);
  const [canview, setCanview] = useState('');
  const [cancreate, setCancreate] = useState('');
  const [canupdate, setCanupdate] = useState('');
  const [candelete, setCandelete] = useState(''); 
  const [roleLists, setRoleLists] = useState([]);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedD: false,
  });


  const handleChangeview = (event) => {
    console.log("create:");

setState({ ...state, [event.target.name]: event.target.checked });
};

  const handleChangecreate = (event) => {
    console.log("create:");

setState({ ...state, [event.target.name]: event.target.checked });
};
const handleChangeupdate = (event) => {
console.log("update:");

setState({ ...state, [event.target.name]: event.target.checked });
};

const handleChangedelete = (event) => {
console.log("create:");

setState({ ...state, [event.target.name]: event.target.checked });
};




  function handleUpdate(){
    const data = {
      role :role,
       can_view:canview,
       can_create:cancreate,
       can_update:canupdate,
       can_delete:candelete,
     }
    console.log("data1",data);
    axios.put(`http://127.0.0.1:8000/tracker/role/${props.recordForEdit}`, data).then((response) => {
      if (response.data.status_code === 200) {
        window.alert(response.data.message);
      }else {
        window.alert(response.data.message);
      } 
    })
  }



  function handleSubmit () {
    const data = {
     role :role,
      can_view:canview,
      can_create:cancreate,
      can_update:canupdate,
      can_delete:candelete,
    }
    axios.put(`http://127.0.0.1:8000/tracker/role/${props.recordForEdit}`, data).then((response) => {
      if (response.data.status_code === 200) {
        window.alert(response.data.message);
        window.location = "/admin"
      }else {
        window.alert(response.data.message);
      } 
    })
    console.log('data',data)

  }


  function handleClickDetails(){
    setOpen(true)
    if(props.recordForEdit != null) {
      axios.get(`http://127.0.0.1:8000/tracker/role/${props.recordForEdit}`).then((response) => {
        if(response.data.status_code === 200) {
          console.log('role', response.data);
          setRole(response.data.result.role);
          console.log("role1",response.data.result.role);

        }
      })
      console.log("edit",recordForEdit)
    }
    else {
      setRole('');
     
    }
  
  }


  return (
    <div>
      <IconButton variant="outlined" color="primary" onClick={handleClickDetails}>
      <EditOutlinedIcon/>

      </IconButton>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
         <strong>Role</strong>
        </DialogTitle>
        <DialogContent dividers>
          
  <form noValidate autoComplete="off" >
       
        <Grid item xs={12} md={12} lg={12}>
          <Grid conatiner item xs={12} md={12} lg={12} className="label">
                    <TextField  
                         id="input-with-icon-grid"
                          label="Role"
                          variant = 'outlined'
                          value = {role}
                          onChange = {(e)=>setRole(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                          </Grid>
                          <Grid>
                          <FormGroup colum>
                <FormControlLabel
                  control={<Checkbox checked={state.checkedA} 
                  // onChange={(e)=>setCanview(e.target.value)}
                  onChange={handleChangeview}

                  name="checkedA" />}
                  label="View"
                  value = {canview}

                />
                <FormControlLabel
                  control={<Checkbox checked={state.checkedB} 
                  // onChange={(e)=>setCancreate(e.target.value)}
                  onChange={handleChangecreate}

                  name="checkedB" />}
                  label="Create"
                  value = {cancreate}

                />
     
                <FormControlLabel
                  control={<Checkbox checked={state.checkedC} 
                  // onChange={(e)=>setCanupdate(e.target.value)}
                  onChange={handleChangeupdate}

                  name="checkedC" />}
                  label="Update"
                  value = {canupdate}
                />
                <FormControlLabel
                  control={<Checkbox checked={state.checkedD} 
                  // onChange={(e)=>setCandelete(e.target.value)}
                  onChange={handleChangedelete}

                  name="checkedD" />}
                  label="Delete"
                  value = {candelete}
                />
                </FormGroup>
      
                          </Grid>
                         </Grid>
                          <Grid className="label">
                          <Button  variant="contained" 
                          color="primary" 
                          onClick = {handleSubmit}
                          >Update
                          </Button>
                          </Grid>
  
    </form>
        </DialogContent>
        <DialogActions>
        
        </DialogActions>
      </Dialog>
    </div>
  );
}























