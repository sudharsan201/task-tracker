import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton } from '@material-ui/core';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { useHistory } from 'react-router-dom';
import Bg from '../../images/TastTrackerBack.png';
import RoleForm  from '../roles/role-update';
import { message } from 'antd';







const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundImage:`url(${Bg})`,
    backgroundRepeat:'no-repeat',
    position:'relative',
    backgroundPosition: "unset",
    backgroundPositionX: "center",
   
  },
  main:{
    display: 'flex',
    alignItems:'center',
    justifyContent:'center',
  },
  image:{
    
   
  },
  label:{
    width:'auto',
  },
  logo:{
    marginTop: '6%',
  },
  head:{
     backgroundPosition:'center',
    width: 'unset !important',
    margin: '0 !important',
    textAlign:'center',
  },
  name:{
    marginBottom: '12px',
    marginTop: '15px',
    
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width:'auto',
    height:'auto',
    overflow: 'hidden',

  },
  icon:{
  },
  margin: {
    margin: theme.spacing(3),
  },
}));



export default function Role(props) {
  
  const classes = useStyles();
  const [roleLists, setRoleLists] = useState([]);
  const [searchValue,setSearchValue]=useState('');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState(false);
  const history = useHistory();
  const [count , setCount] = useState()
  const [role, setRole] = useState([]);
  const [view, setView] = useState('False');
  const [create, setCreate] = useState('False');
  const [update, setUpdate] = useState('False');
  const [Delete, setDelete] = useState('False');
  const [refresh, setRefresh] = useState(false)

  const sNO=1


    const [state, setState] = React.useState({
        checkedA: false,
        checkedB: false,
        checkedC: false,
        checkedD: false,
        checkedE: false,
        checkedF: false,
        checkedG: false,
        checkedH: false,
      });

  
    const handleChangePage = (event, newPage) => {
      console.log(newPage)
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(1);
    };

    const handleChange = (e, value) => {
      e.preventDefault();
      if (value % 2) {
        console.log('is_full_day:', 'True');
        setView('True');
      } else {
        console.log('is_full_day:', 'False');
        setView('False');
      }
    };
    
    const handleChangecreate = (e, value) => {
      e.preventDefault();
      if (value % 2) {
        console.log('is_full_day:', 'True');
        setCreate('True');
      } else {
        console.log('is_full_day:', 'False');
        setCreate('False');
      }
    };

    const handleChangeUpdate = (e, value) => {
      e.preventDefault();
      if (value % 2) {
        console.log('is_full_day:', 'True');
        setUpdate('True');
      } else {
        console.log('is_full_day:', 'False');
        setUpdate('False');
      }
    };
    const handleChangeDelete = (e, value) => {
      e.preventDefault();
      if (value % 2) {
        console.log('is_full_day:', 'True');
        setDelete('True');
      } else {
        console.log('is_full_day:', 'False');
        setDelete('False');
      }
    };
  

   
    
  const handleChangeupdate = (event) => {
    console.log("update:");

setState({ ...state, [event.target.name]: event.target.checked });
};

const handleChangedelete = (event) => {
  console.log("create:");

setState({ ...state, [event.target.name]: event.target.checked });
};

const handleChange1 = (event) => {
  console.log("view:",state.name);
    setState({ ...state, [event.target.name]: event.target.checked });
};

const handleChange2 = (event) => {
        console.log("create:");

  setState({ ...state, [event.target.name]: event.target.checked });
};
const handleChange3 = (event) => {
console.log("update:");

setState({ ...state, [event.target.name]: event.target.checked });
};

const handleChange4 = (event) => {
console.log("create:");

setState({ ...state, [event.target.name]: event.target.checked });
};


    function handleUpdate(){
      history.push('/user-update');
      setOpen(true);
    }



     function handleSubmit () {
       const data = {
        role :role,
         can_view:view,
         can_create:create,
         can_update:update,
         can_delete:Delete,
       }
       console.log(data)
       axios.post(`http://127.0.0.1:8000/tracker/role/`,data).then((response)=>{
        if(response.data.status_code === 201){
          message.success(response.data.message);
        }else{
          message.error(response.data.message);
        }
      })
    }
    

    useEffect(()=>{
      axios.get(`http://127.0.0.1:8000/tracker/role/`).then((response)=>{
        if(response.data.status_code === 200){
          setRoleLists(response.data.results);
        }else{
      console.log('alert',response.message)
        }
      })
    },[])



    function handleDeleted(data){
      axios.delete(`http://127.0.0.1:8000/tracker/role/${data.id}`).then((response)=>{
          if(response.status_code === 404){
            window.alert(response.data.message);  
          }else{
              window.alert(response.data.message);
          }
      })
    }  
  return (
    <div className={classes.root}>
      <Grid spacing={2} className={classes.head}>
        <Grid item xs={12} md={12} lg={12} className={classes.main}>
            <Grid item xs={12} md={6} lg={6} className={classes.margin}>
           <Grid item xs={6} md={6} lg={4} className={classes.name}>
                <Typography variant="h4">Role</Typography>
            </Grid>
               <Grid item xs={10} md={10} lg={4} className={classes.name}>
                        <TextField className={classes.label}
                         id="input-with-icon-grid"
                          label="Role"
                          variant = 'outlined'
                          value = {role}
                          onChange = {(e)=>setRole(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
               </Grid>
               <Grid item xs={6} md={4} lg={4} className={classes.lable}>
               <FormGroup>
                <FormControlLabel
                   name 
                   control={<Checkbox onChange={handleChange}/>}
                   label='view'
                />
                <FormControlLabel
                  name 
                  control={<Checkbox onChange={handleChangecreate}/>}
                  label='Create'

                />
                <FormControlLabel
                  control={<Checkbox onChange={handleChangeUpdate}/>}
                  label="Update"
                />
                <FormControlLabel
                  control={<Checkbox onChange={handleChangeDelete}/>}
                  label="Delete"
                />
                </FormGroup>
               </Grid>
                    <Grid item xs={4} md={4} lg={4} className={classes.name} >
                    <Button  
                    variant="contained" 
                    color="primary" 
                    size="large"
                    style={{color:"#fff",fontWeight:"bold",marginTop:"10px"}}
                    onClick = {handleSubmit}
                    >
                      Submit
                    </Button>
                    </Grid>
                    </Grid>
                    </Grid>
        <Grid container xs={12} md={12} lg={12} spacing={3}>
        <Grid item xs={4} md={4} lg={2}>
        <TextField className={classes.name} 
                         id="input-with-icon-grid"
                          label="Search"
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                        </Grid>
                        </Grid>
        <Grid item xs={12} md={12} lg={12}>
        <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table>
          <TableHead>
            <TableRow style={{backgroundColor:"darkgray"}}>
                <TableCell> S.No</TableCell>
                <TableCell> Role</TableCell>
                <TableCell> View</TableCell>
                <TableCell> Create</TableCell>
                <TableCell> Update</TableCell>
                <TableCell> Delete</TableCell>
                <TableCell> Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {roleLists.map((item, index) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={item.id}>
                      <TableCell>{index+sNO}</TableCell>
                      <TableCell>{item.role}</TableCell>
                        <TableCell>
                          {item.can_view}
                          <FormControlLabel
                            name 
                            control={<Checkbox onChange={handleChange}/>}
                            />
                        </TableCell>
                        <TableCell>
                          {item.can_create}
                          <FormControlLabel
                            name 
                            control={<Checkbox onChange={handleChangecreate}/>}
                          />
                          </TableCell>
                        <TableCell>
                          {item.can_update}
                          <FormControlLabel
                            control={<Checkbox onChange={handleChangeUpdate}/>}
                          />
                          </TableCell>
                        <TableCell>
                          {item.can_delete}
                          <FormControlLabel
                            control={<Checkbox onChange={handleChangeDelete}/>}
                          />
                          </TableCell>
                        <TableCell>
                        <IconButton
                        color='primary'
                        >
                          <RoleForm
                          recordForEdit={item.id}/>
                          </IconButton>
                        <IconButton
                         color='primary'
                         onClick = {()=>{handleDeleted(item)}}
                        >
                          <DeleteForeverOutlinedIcon/>
                        </IconButton>
                      </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
        </Grid>
         </Grid>
    </div>
  );
}






