import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Image from '../../images/404-error.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    backgroundImage: `url(${Image})`,
    width:'auto',
    height:'1000px',
    backgroundSize: 'cover',
    overflow: 'hidden',

  },
}));

export default function BadRequest404() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>404 Page  Bad Request</Paper>
        </Grid>

      </Grid>
    </div>
  );
}