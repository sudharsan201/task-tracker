import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Image from '../../images/401-error.png';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    backgroundImage: `url(${Image})`,
    width:'auto',
    height:'1000px',
    backgroundSize: 'cover',
    overflow: 'hidden',

  },
}));

export default function Unauthorized401() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>Unauthorized 401</Paper>
        </Grid>

      </Grid>
    </div>
  );
}