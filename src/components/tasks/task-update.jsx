import Styles from './style.css';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { TextField , Select} from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import Autocomplete from '@material-ui/lab/Autocomplete';


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  


});
const useStyles = makeStyles((theme) => ({
    root: {
       
       backgroundRepeat:'no-repeat',
       position:'relative',
       backgroundPosition: "unset",
       backgroundPositionX: "center",
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 300,
      },
  
    },
    logo:{
      marginTop: '6%',
    },
    TableHead:{
      top: 0,
      left: 0,
      zIndex: 2,
      Position: 'sticky',
      BackgroundColor: 'black',
    
     
    },
    name:{
      
      marginBottom: '12px',
      marginTop: '15px',
    },
    label:{
      width: '480px !important',
    },
    paper: {
      
      textAlign: 'center',
      color: theme.palette.text.secondary,
      width:'auto',
      height:'auto',
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'center',
  
    },
    margin: {
      margin: theme.spacing(1),
    },
    formControl: {
      width: '30Vh',
      marginBottom: '12px',
      textAlign: 'center',
      marginTop:'10px',
  
    },
    weblable:{
      width: '30Vh',
      marginBottom: '12px',
      textAlign: 'center',
      marginTop:'10px',

    }
    
  }));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);












export default function TaskForm(props) {
  const { addorEdit, recordForEdit}  = props

  const [open, setOpen] = React.useState(false);
  const [website, setWebsite]=useState();
    const [description, setDescription]=useState([]);
    const [reportingdate, setReportingdate]=useState();
    const [expectedclosuredate, setExpectedclosuredate]=useState();
    const [taskType, setTaskType]= useState();
    const [status, setStatus]= useState();
    const [role,setRole]=useState([]);
    const [search,setSearch]= useState();
    const classes = useStyles();
    const [websitelist,setwebsitelist]=useState([]);
    const [assignedto, setAssignedto] =useState([]);
    const [task, setTask]=  useState('');
    const [pdfFileError, setpdfFileError] = useState('');
    const [file, setFile] = useState('');
    const [userS, setUsers] = useState([]);
    const [refresh, setRefresh] = useState(false)

  const Role = localStorage.getItem('Role');
  // const userName = localStorage.getItem('User');

    const fileType = ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'];
    const handlepdfFileChange = (e) => {
      let selectedFile = e.target.files[0];
      console.log("file details :", selectedFile)
      if (selectedFile) {
        if (selectedFile && fileType.includes(selectedFile.type)) {
          let reader = new FileReader();
          reader.readAsDataURL(selectedFile);
          reader.onloadend = (e) => {
            setFile(selectedFile);
            console.log("fileupload", e.target.result)
            setpdfFileError('');
          }

        }
        else {
          setpdfFileError('Please select valid pdf file');
        }
      }
      else {
        console.log('select your file');
      }

    }
  
 


    

  



  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  function handleWebsiteValues(event, values) {
    console.log("website value:", values)
    values.map((item) => {
      website.push(item.id);

    }

    )

  }

 
    function handleSubmit(){
      const data = {
        description: description,
        website:[...new Set(website)],
        reporting_date: reportingdate,
        expected_closure_date: expectedclosuredate,
        task_type: taskType,
        assigned_to: parseInt(assignedto),
        status: status,
        file:file,
    }
    console.log("data",data);
      // window.alert(props.recordForEdit);
      axios.put(`http://127.0.0.1:8000/tracker/task/${props.recordForEdit}`, data).then((response) => {
        setRefresh(!refresh);
        if (response.data.status_code === 200) {
          message.success(response.data.message);
          handleClose()
          // window.location = "/admin"
        }else {
          message.error(response.data.message);
        } 
      })
    
    }


  function handleClickDetails(){
    setOpen(true)
    axios.get(`http://127.0.0.1:8000/tracker/task/${props.recordForEdit}`).then((response)=>{
      console.log("response-tasks:",response);
      if(response.data.status_code===200){
        setTask(response.data.results);
      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/employee/`).then((response) => {
      console.log("response-user:", response);
      if (response.data.status_code === 200) {
        setUsers(response.data.results);
      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/website/`).then((response) => {
      console.log("response-website:", response);
      if (response.data.status_code === 200) {
        setwebsitelist(response.data.results)
      }
    })
    if (props.recordForEdit){
      axios.get(`http://127.0.0.1:8000/tracker/task/${props.recordForEdit}`).then((response)=>{
          if(response.status===200){
              console.log('response:',response.data.result.website[0].websitename);
              setWebsite(response.data.result.website[0].websitename);
              console.log("Description",response.data.result.description)
              setDescription(response.data.result.description);
              setTaskType(response.data.result.task_type);
              setReportingdate(response.data.result.reporting_date);
              setExpectedclosuredate(response.data.result.expected_closure_date);
              setAssignedto(response.data.result.assigned_to.id);
              setStatus(response.data.result.status);
              setFile(response.data.result.assigned_to.file);
              console.log("file",response.data.result.file);
          }
      })
    }
    else{
      setWebsite('');
      setDescription('');
      setTaskType('');
      setReportingdate('');
      setExpectedclosuredate('');
      setAssignedto('');
      setStatus('');
      setFile('');
    }
  }
  
  // function saveAssginedto(e){
  //   console.log(e.target.value);
  //   setAssignedto(e.target.value);
  // }

  return (
    <div>
      <IconButton variant="outlined" color="primary" onClick={handleClickDetails}>
      <EditOutlinedIcon/>
      </IconButton>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
         <strong></strong>
        </DialogTitle>
        <DialogContent dividers>   
      <form noValidate autoComplete="off" >
        <Grid item xs={12} md={12} lg={12} >
          <Grid conatiner item xs={12} md={12} lg={12} className="label">
                          <Grid className="label">
                        <TextField 
                         id="input-with-icon-grid"
                         name
                          label="Description"
                          variant = 'outlined'
                          value = {description}
                          onChange = {(e)=>setDescription(e.target.value)}
                          InputProps={{
                            startAdornmDescriptionent: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                          </Grid>
                          {Role && Role === 'Admin'?                  
                          <Grid className="label">
                           <TextField
                         id="input-with-icon-grid"
                          label="Tasktype"
                          variant = 'outlined'
                          value = {taskType}
                          onChange = {(e)=>setTaskType(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                          </Grid>
                          :''}
                          </Grid>
                          </Grid>
                          <>  
                          {Role && Role === 'Admin'?    
                          <Grid container xs={12} md={12} lg={12}>
                        <Grid item xs={12} md={12} lg={12}>
                        {/* <Grid item xs={6} md={4} lg={4} spacing={4}>
                          <Autocomplete className={classes.weblable}
                            fullWidth
                            multiple
                            id="tags-outlined"
                            options={websitelist}
                            getOptionLabel={(option) => option.websitename}
                            defaultValue={website}
                            filterSelectedOptions
                            onChange={handleWebsiteValues}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                variant="outlined"
                                label="Website"
                                placeholder="Websites"
                                margin="normal"
                                fullWidth
                              />
                            )}
                          />
                        </Grid> */}
                        <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">Website</InputLabel>
                        <Select
                          native
                          value={[...new Set(website)]}
                          onChange={(e)=>setWebsite(e.target.value)}
                          label="website"
                          inputProps={{
                            name: 'website',
                            id: 'outlined-age-native-simple',
                          }}
                        >
                        <option value={[...new Set(website)]}>{website}</option> 
                      </Select>
                      </FormControl>
                      </Grid>
                      </Grid>
                      :''}
                      </>
                      {Role && Role === 'Admin'? 
                        <Grid container xs={12} md={12} lg={12}>
                        <Grid item xs={12} md={12} lg={12}>
                        <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">Assigned To</InputLabel>
                        <Select
                          native
                          value={assignedto}
                          onChange={(e)=>setAssignedto(e.target.value)}
                          label="assignedto "
                          inputProps={{
                            name: 'assignedto',
                            id: 'outlined-age-native-simple',
                          }}
                        >
                          {userS && userS.map((user) =>
                          <option value={user.id}>{user.employee_name.username}</option>)}
                      </Select>
                      </FormControl>
                      </Grid> 
                      </Grid>
                      :''}
                          <Grid container xs={12} md={12} lg={12}>
                        <Grid item xs={12} md={12} lg={12}>
                        <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">Status</InputLabel>
                        <Select
                          native
                          value={status}
                          onChange={(e)=>setStatus(e.target.value)}
                          label="status"
                          inputProps={{
                            name: 'status',
                            id: 'outlined-age-native-simple',
                          }}
                        >
                          <option defaultValue="Hold" style={{color:"red"}}>Hold</option>
                          <option value="In progres" style={{color:"blue"}}>In Progres</option>
                          <option value="Completed" style={{color:"green"}}>Completed</option>
                      </Select>
                      </FormControl>
                      </Grid>
                      </Grid>
                      {Role && Role === 'Admin'? 
                      <Grid className="label">
                      <TextField
                      fullWidth
                      id="date"
                      label="Reporting date"
                      type="date"
                      variant="outlined"
                      defaultValue="2021-08-13"
                      value = {reportingdate}
                      onChange = {(e)=>setReportingdate(e.target.value)}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                          </Grid>
                          :''}
                          <>
                          {Role && Role === 'Admin'? 
                          <Grid className="label">
                      <TextField
                      fullWidth
                      id="date"
                      label="ExpectetdClosure date"
                      type="date"
                      variant="outlined"
                      defaultValue="2021-08-13"
                      value = {expectedclosuredate}
                      onChange = {(e)=>setExpectedclosuredate(e.target.value)}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                          </Grid>
                          :''}

                          </>
                          <Grid item xs={4} md={4} lg={4}>
                          <Grid container direction='row' style={{marginTop:'10px'}}>
                            <Grid item xs={2} md={6} lg={2} style={{paddingLeft:'10px'}}>
                              <input type='file'  required onChange={handlepdfFileChange} />{file} {pdfFileError && <div className='error'>{pdfFileError}</div>}
                            </Grid>
                            </Grid>
                            </Grid>
                          <Grid className="label">
                          <Button  variant="contained" 
                          color="primary" 
                          style={{marginTop:"15px", marginLeft:"30%"}}
                          onClick = {handleSubmit}
                          >Update</Button>
                      </Grid>
                </form>
        </DialogContent>
        <DialogActions>
        </DialogActions>
      </Dialog>
    </div>
  );
}






















