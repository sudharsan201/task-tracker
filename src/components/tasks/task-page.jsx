import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton, Select } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import tasks from '../../config/services';
import Bg from '../../images/TastTrackerBack.png';
import { useHistory } from 'react-router-dom';
import TaskForm from '../tasks/task-update';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';
// import { Viewer } from '@react-pdf-viewer/core'; // install this library
// Plugins
// import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout'; // install this library
// Import the styles
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
// Worker
// import { Worker } from '@react-pdf-viewer/core'; // install this library
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});









const useStyles = makeStyles((theme) => ({
  root: {
    backgroundImage: `url(${Bg})`,

    backgroundRepeat: 'no-repeat',
    position: 'relative',
    backgroundPosition: "unset",
    backgroundPositionX: "center",
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: " 100%",
    },
    flexGrow: 1,


  },
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {


  },
  label: {
    width: '370px !important',

  },
  logo: {
    marginTop: '6%',
  },
  head: {
    backgroundRepeat: 'no-repeat',
    position: 'relative',
    backgroundPosition: 'center',
    width: 'unset !important',
    margin: '0 !important',
  },
  name: {
    marginBottom: '12px',
    marginTop: '15px',
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: 'auto',
    height: 'auto',
    overflow: 'hidden',

  },
  icon: {

  },
  margin: {
    margin: theme.spacing(1),
  },
  formControl: {
    width: '70%',
    marginBottom: '12px',
  },
  error: {
    width: "100%",
    color: "red",
    fontWeight: "14px",
    fontsize: "600",

  }
}));

export default function Task() {
  const classes = useStyles();
  const [taskLists, setTaskLists] = useState([]);
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [count, setCount] = useState()
  const [open, setOpen] = React.useState(false);
  const history = useHistory();
  const [website, setWebsite] = useState([]);
  const [description, setDescription] = useState('');
  const [reportingdate, setReportingdate] = useState('');
  const [expectedclosuredate, setExpectedclosuredate] = useState('');
  const [tasktype, setTasktype] = useState('');
  const [assignedto, setAssignedto] = useState([]);
  const [status, setStatus] = useState('');
  const [search, setSearch] = useState('');
  const [userS, setUsers] = useState([]);
  const [websitelist, setwebsitelist] = useState([]);
  //for onChange event
  // const [pdfFile, setpdfFile] = useState(null);
  const [pdfFileError, setpdfFileError] = useState('');
  // for Submit Event
  // const [viewPdf, setViewpdf] = useState('');
  // const defaultLayoutPluginInstance = defaultLayoutPlugin();
  const [file, setFile] = useState(null);
  const [refresh, setRefresh] = useState(false)
  const sNo = 1;
  const [close,setClose] = React.useState(false)

  console.log("test file :", file);

  //for onChange event
  const fileType = ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'];
  const handlepdfFileChange = (e) => {
    let selectedFile = e.target.files[0];
    console.log("file details :", selectedFile)
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsDataURL(selectedFile);
        reader.onloadend = (e) => {
          setFile(selectedFile);
          console.log("fileupload", e.target.result)
          setpdfFileError('');
        }

      }
      else {
        setFile(null);
        setpdfFileError('Please select valid pdf file');
      }
    }
    else {
      console.log('select your file');
    }

  }

  // const handlepdfFileSubmit = (e) => {
  //   e.preventDefault();
  //   if (pdfFile !== null) {
  //     setViewpdf(pdfFile);
  //   }
  //   else {
  //     setViewpdf(null);
  //   }
  // }




  const Role = localStorage.getItem('Role');
  const userName = localStorage.getItem('User');




  const handleChangePage = (event, newPage) => {
    console.log(newPage)
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(event.target.value);
    setPage(1);
  };


  function handleUpdate() {
    history.push('/user-update');
    setOpen(true);
  }

  function handleClosetask(){
    setOpen(true);

  }

  function handleClickOpentask(){
    setOpen(true);
    
  }

  function handleClose(){
    setClose(true);
    
  }
  const handleDelete =(data) => {
    handleDeleted(data)
    
}



  function handleDeleted(data) {
    axios.delete(`http://127.0.0.1:8000/tracker/task/${data.id}`).then((response) => {
      setRefresh(!refresh)
      if (response.status_code === 404) {
        message.error(response.data.message);
      } else {
        message.success(response.data.message);
      }
    })
  }


  function handleSubmit() {
    if (![...new Set(website)]) {
      message.warning('Please Enter website');
    }
    else if (!description) {
      message.warning('Please Enter Description');
    }
    else if (!reportingdate) {
      message.warning('Please Select Reporting Date');
    }
    else if (!expectedclosuredate) {
      message.warning('Please Select Expected Closure Date');
    }
    else if (!tasktype) {
      message.warning('Please Enter  TaskType');
    }
    else if (!status) {
      message.warning('Please Select Status');
    }
    else if (!assignedto) {
      console.log('assigned to', assignedto)
      message.warning('Please Select User');
    }
    else if (!file) {
      console.log('file', file)
      message.warning('Please Select file');
    }
    else if ([...new Set(website)] && description && reportingdate && expectedclosuredate && tasktype && status && assignedto && file) {
      const data = {
        website: [...new Set(website)],
        description: description,
        reporting_date: reportingdate,
        expected_closure_date: expectedclosuredate,
        task_type: tasktype,
        assigned_to: parseInt(assignedto),
        status: status,
        file: file,
      }
      var formData = new FormData();
      formData.append("website", [...new Set(website)]);
      formData.append("description", description);
      formData.append("reporting_date", reportingdate);
      formData.append("expected_closure_date", expectedclosuredate);
      formData.append("task_type", tasktype);
      formData.append("assigned_to", parseInt(assignedto));
      formData.append("status", status);
      formData.append("file", file);
      console.log("data:", data)

      axios.post(`http://127.0.0.1:8000/tracker/task/`, formData).then((response) => {
        console.log("response:post", response)
        setRefresh(!refresh)
        if (response.data.status_code === 201) {
          message.success(response.data.message);
          setWebsite(['']);
          setDescription('');
          setReportingdate('');
          setExpectedclosuredate('');
          setTasktype('');
          setStatus('');
          setAssignedto('');
          setFile('');

        } else {
          message.error(response.data.message);
        }
      })
    }

  }

  function getTaskList(page, rowsPerPage) {
    axios.get(`${tasks.tasks.taskList}?page_size=${rowsPerPage}&page=${page}`).then((response) => {
      if (response.data.status_code === 200) {
        setTaskLists(response.data.results);
        setCount(response.data.count);
        console.log(response.data.results.map((item) => item.website.map((web) => web?.websitename)));
      } else {
        console.log('alert', response.message)
      }
    })
  }
  useEffect(() => {
    setPage(1)
    getTaskList(page, rowsPerPage);
    axios.get(`http://127.0.0.1:8000/tracker/employee/`).then((response) => {
      console.log("response-user:", response);
      if (response.data.status_code === 200) {
        setUsers(response.data.results);
      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/website/`).then((response) => {
      console.log("response-website:", response);
      if (response.data.status_code === 200) {
        setwebsitelist(response.data.results)
      }
    })
  }, [page, rowsPerPage, refresh])


  function handleWebsiteValues(event, values) {
    console.log("website value:", values)
    values.map((item) => {
      website.push(item.id);

    }

    )

  }


  useEffect(() => {
    console.log("users:", userS)
  }, [userS])

  // const [data,setData] = useState([]);

  // useEffect(()=>{
  //   console.log("search",search);
  //   const newData = taskLists.filter(description=>description.toLowerCase().include(search.toLowerCase()))

  // },[search]);


  return (
    <div className={classes.root}>
      <Grid>
        <form className={classes.root} noValidate autoComplete="off">
          <Typography variant="h4">Tasks</Typography>
          {Role && Role === 'Admin' ?
            <Grid container item xs={12} md={12} lg={12} spacing={3}>
              <Grid item xs={4} md={4} lg={4}>
                <TextField className={classes.label}
                  id="input-with-icon-grid"
                  label="TaskDescription"
                  variant='outlined'
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={4} md={4} lg={4}>
                <TextField className={classes.label}
                  id="input-with-icon-grid"
                  label="TaskType"
                  variant='outlined'
                  value={tasktype}
                  onChange={(e) => setTasktype(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item xs={2} md={2} lg={2} spacing={4}>
                <TextField className={classes.labeldate}
                  fullWidth
                  id="date"
                  label="Reporting date"
                  type="date"
                  variant="outlined"
                  defaultValue="2021-08-13"
                  value={reportingdate}
                  onChange={(e) => setReportingdate(e.target.value)}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={2} md={2} lg={2} spacing={4}>
                <TextField className={classes.labeldate}
                  fullWidth
                  id="date"
                  label="Expected Closure date"
                  type="date"
                  variant="outlined"
                  defaultValue="2021-08-13"
                  value={expectedclosuredate}
                  onChange={(e) => setExpectedclosuredate(e.target.value)}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={4} md={4} lg={4}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel htmlFor="outlined-age-native-simple">Assigned To</InputLabel>
                  <Select
                    native
                    value={assignedto}
                    onChange={(e) => setAssignedto(e.target.value)}
                    label="assignedto "
                    inputProps={{
                      name: 'assignedto',
                      id: 'outlined-age-native-simple',
                    }}
                  >
                    <option aria-label="None" value="" />
                    <option> </option>
                    {userS && userS.map((user) =>
                      <option value={user.id}>{user.employee_name.username}</option>)}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={4} md={4} lg={4}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel htmlFor="outlined-age-native-simple">Status</InputLabel>
                  <Select
                    native
                    value={status}
                    onChange={(e) => setStatus(e.target.value)}
                    label="status"
                    inputProps={{
                      name: 'status',
                      id: 'outlined-age-native-simple',
                    }}
                  >
                    <option aria-label="None" value="" />
                    <option> </option>
                    <option defaultValue="Hold" style={{ color: "red" }}>Hold</option>
                    <option value="In progres" style={{ color: "blue" }}>In Progres</option>
                    <option value="Completed" style={{ color: "green" }}>Completed</option>

                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} md={4} lg={4} spacing={4}>
                <Autocomplete
                  fullWidth
                  multiple
                  id="tags-outlined"
                  options={websitelist}
                  getOptionLabel={(option) => option.websitename}
                  defaultValue={website}
                  filterSelectedOptions
                  onChange={handleWebsiteValues}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      label="Website"
                      placeholder="Websites"
                      margin="normal"
                      fullWidth
                    />
                  )}
                />
              </Grid>
            </Grid>
            : ''}
          <Grid container item xs={12} md={12} lg={12} spacing={3}>
            <Grid item xs={4} md={4} lg={4}>
            </Grid>
            <Grid item xs={4} md={4} lg={4}>
            </Grid>
            <Grid item xs={4} md={4} lg={4}>
              <Grid container direction='row' style={{marginTop:'10px'}}>
              {Role && Role ==='Admin'? 

                <Grid item xs={2} md={6} lg={2} style={{paddingLeft:'10px'}}>
                  <input type='file' required onChange={handlepdfFileChange} /> {pdfFileError && <div className='error'>{pdfFileError}</div>}
                </Grid>
                  :''}
                <Grid item xs={2} md={6} lg={2} style={{ marginLeft: "350px", color: '#fff', fontWeight: "bold" }}>
                {Role && Role ==='Admin'? 
                  <Button variant="contained" color="primary"
                    onClick={handleSubmit}
                  >Submit
                      </Button>
                      :''}
                </Grid>
              </Grid>
            </Grid>

          </Grid>


        </form>

        {/* <h4> ViewPDF</h4> */}
{/* 
        <div className='pdf-container'>
          {viewPdf && <><Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
            <Viewer fileUrl={viewPdf}
              plugins={[defaultLayoutPluginInstance]} />
          </Worker></>}
          {!viewPdf && <></>}
        </div> */}
        <Grid container xs={12} md={12} lg={8} spacing={3}>
          <Grid item xs={4} md={4} lg={4}>
            {}
            <TextField className={classes.label}
              id="input-with-icon-grid"
              label="Search"
              variant='outlined'
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} md={12} lg={12}>
          <Paper style={{ backgroundColor: "unset" }}>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow style={{ backgroundColor: "darkgray", color: "#fff", fontWeight: "bold" }}>
                    <TableCell> S.No</TableCell>
                    <TableCell> Website</TableCell>
                    <TableCell> Task Description</TableCell>
                    <TableCell> Reporting Date</TableCell>
                    <TableCell> Expected ClosureDate </TableCell>
                    <TableCell> Assigned To</TableCell>
                    <TableCell> Task Type </TableCell>
                    <TableCell> Status</TableCell>
                    <TableCell> File</TableCell>
                    <TableCell> Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {taskLists.map((item, index) => {
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={item.id}>
                        {Role && Role === 'Admin' || userName === item.assigned_to?.employee_name?.username ?
                          <>
                            <TableCell>{index + sNo}</TableCell>
                            <TableCell>
                              {item.website.map((web) =>
                                <>
                                  {web.websitename ?
                                    <p>{web?.websitename},<br /></p>
                                    :
                                    <p>''</p>
                                  }
                                </>
                              )}
                            </TableCell>
                            <TableCell>{item.description}</TableCell>
                            <TableCell>{item.reporting_date}</TableCell>
                            <TableCell>{item.expected_closure_date}</TableCell>
                            <TableCell>{item.assigned_to?.employee_name?.username}</TableCell>
                            <TableCell>{item.task_type}</TableCell>
                            <TableCell>{item.status}</TableCell>
                            <TableCell><a href={'http://127.0.0.1:8000' + String(item.file)} download>Download</a></TableCell>
                              <TableCell>
                                <IconButton
                                  color='primary'
                                >
                                  <TaskForm
                                    recordForEdit={item.id}
                                  />
                                </IconButton>
                                {Role && Role === 'Admin' ? 
                                <IconButton
                                  color='primary'
                                  onClick={() => { handleDelete(item) }}
                                >
                                <Button variant="outlined" onClick={handleClickOpentask}>
                                <DeleteForeverOutlinedIcon />
                                </Button>
                                <Dialog
                                  open={open}
                                  TransitionComponent={Transition}
                                  keepMounted
                                  onClose={handleClosetask}
                                  aria-describedby="alert-dialog-slide-description"
                                >
                                  <DialogTitle>{"Use Google's location service?"}</DialogTitle>
                                  <DialogContent>
                                    <DialogContentText id="alert-dialog-slide-description">
                                    Are you sure you want to delete this Task?
                                    </DialogContentText>
                                  </DialogContent>
                                  <DialogActions>
                                    <Button onClick={handleClose}>YES</Button>
                                    <Button onClick={handleClosetask}>NO</Button>
                                  </DialogActions>
                                </Dialog>
                                </IconButton>
                                  : ''}
                              </TableCell>
                            
                          </>
                          : ''}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={count}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}






