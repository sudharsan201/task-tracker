import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton } from '@material-ui/core';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import website from '../../config/services';
import InputAdornment from '@material-ui/core/InputAdornment';
import Bg from '../../images/TastTrackerBack.png';
import { useHistory } from 'react-router-dom';
import WebsiteForm from './website-update';
import { message } from 'antd';



const useStyles = makeStyles((theme) => ({
  root: {
    backgroundImage:`url(${Bg})`,
     backgroundRepeat:'no-repeat',
     position:'relative',
     backgroundPosition: "unset",
     backgroundPositionX: "center",
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: 200,
    },
    flexGrow: 1,
   
   
  },
  main:{
    display: 'flex',
    alignItems:'center',
    justifyContent:'center',
  },
  image:{
    
   
  },
  label:{
    width: '140% !important',
  },
  logo:{
    marginTop: '6%',
  },
  head:{
    
    backgroundPosition:'center',
    width: 'unset !important',
    margin: '0 !important',
  },
  name:{
    marginBottom: '12px',
    marginTop: '15px',
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width:'auto',
    height:'auto',
    overflow: 'hidden',

  },
  icon:{
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function Website() {
  const classes = useStyles();
  const [websiteLists, setWebsiteLists] = useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [count , setCount] = useState()
    const [open, setOpen] = React.useState(false);
    const history = useHistory();
    const [websitename, setWebsitename] = useState();
    const [search, setSearch] = useState();
    const sNO=1;
    const [refresh, setRefresh] = useState(false)

    
  
    const handleChangePage = (event, newPage) => {
      console.log(newPage)
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(1);
    };

    function handleDeleted(data){
      axios.delete(`http://127.0.0.1:8000/tracker/website/${data.id}`).then((response)=>{
        setRefresh(!refresh)
          if(response.status_code === 404){
            window.alert(response.data.message);  
          }else{
              window.alert(response.data.message);
          }
      })  
    }  


    

    function handleUpdate(){
      history.push('/user-update');
      setOpen(true);
    }
    function handleSubmit(){
      const data = {
        websitename:websitename,
      }
      axios.post(`http://127.0.0.1:8000/tracker/website/`,data).then((response)=>{
        setRefresh(!refresh)
        console.log("response:post",response)
        if(response.data.status_code === 201){
          message.success(response.data.message);

        }else{
          message.error(response.data.message);
        }
          })
        }


  
    useEffect(()=>{
      setPage(1)
      axios.get(`${website.website.websiteList}?page_size=${rowsPerPage}&page=${page}`).then((response)=>{
        console.log(response.data)
        if(response.data.status_code === 200){
          setWebsiteLists(response.data.results);
          setCount(response.data.count);
        }else{
      console.log('alert',response.message)
        }
      })
    },[page,refresh])


  return (
    <div className={classes.root}>
      <Grid spacing={2} className={classes.head}>
         
        <Grid item xs={12} md={12} lg={12} className={classes.main}>
          <Grid item xs={12} md={6} lg={6} className={classes.logo}>
          </Grid>
        <Grid item xs={12} md={6} lg={6} className={classes.margin}>
           <Grid item xs={6} md={6} lg={6} className={classes.name}>
                <Typography variant="h4">Website</Typography>
            </Grid>
               <Grid item xs={6} md={6} lg={4} className={classes.name}>
                        <TextField className={classes.label}
                         id="input-with-icon-grid"
                          label="Website"
                          variant = 'outlined'
                          value = {websitename}
                          onChange = {(e)=>setWebsitename(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
               </Grid>
                    <Grid item xs={10} md={10} lg={6} className={classes.name}>
                      <Button  variant="contained" color="primary" size="large"style={{color:"#fff",fontWeight:"bold",marginTop:"20px"}}
                      onClick = {handleSubmit}
                      >Submit</Button>
                    </Grid>
        </Grid>
        </Grid>
        <br/>
        <br/>
       
        <Grid container xs={12} md={12} lg={8} spacing={3}>
        <Grid item xs={4} md={4} lg={2} >
        <TextField className={classes.lable} 
                         id="input-with-icon-grid"
                          label="Search"
                          variant = 'outlined'
                          value = {search}
                          onChange = {(e)=>(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                 
              </Grid>
              </Grid>
       
        <Grid item xs={12} md={12} lg={12}>
        <Paper>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow style={{backgroundColor:"darkgray"}}>
                <TableCell> S.No</TableCell>
                <TableCell> Website</TableCell>
                <TableCell> Actions</TableCell>
                    </TableRow>
          </TableHead>
          <TableBody>
            {websiteLists.map((item, index) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={item.id}>
                      <TableCell>{index +sNO}</TableCell>
                      <TableCell>{item.websitename}</TableCell>
                      <TableCell>
                        <IconButton
                        color='primary'
                        >
                          <WebsiteForm
                          recordForEdit={item.id}
                         />
                        </IconButton>
                        <IconButton
                         color='primary'
                         onClick = {()=>{handleDeleted(item)}}
                        >
                          <DeleteForeverOutlinedIcon/>
                        </IconButton>
                      </TableCell>

                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
        </Grid>
        </Grid>
     
    </div>
  );
}






