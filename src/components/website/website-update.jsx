import Styles from './style.css';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { TextField } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import Website from "../website/website-page";

import axios from 'axios'


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  


});
const useStyles = makeStyles((theme) => ({
    root: {
       
       backgroundRepeat:'no-repeat',
       position:'relative',
       backgroundPosition: "unset",
       backgroundPositionX: "center",
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 300,
      },
  
    },
    logo:{
      marginTop: '6%',
    },
    TableHead:{
      top: 0,
      left: 0,
      zIndex: 2,
      Position: 'sticky',
      BackgroundColor: 'black',
    
     
    },
    name:{
      
      marginBottom: '12px',
      marginTop: '15px',
    },
    label:{
      width: '480px !important',
    },
    paper: {
      
      textAlign: 'center',
      color: theme.palette.text.secondary,
      width:'auto',
      height:'auto',
      overflow: 'hidden',
      alignItems: 'center',
      justifyContent: 'center',
  
    },
    margin: {
      margin: theme.spacing(1),
    },
    
  }));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function UserForm(props) {
  const { addorEdit, recordForEdit } = props

  const [open, setOpen] = React.useState(false);

  const [websitename, setwebsitename] = useState();
  const [website, setwebsite]= useState();
 
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };


  function handleSubmit(){
    const data = {
      websitename:website,
     
    }
    // window.alert(props.recordForEdit);
        axios.put(`http://127.0.0.1:8000/tracker/website/${props.recordForEdit}`, data).then((response) => {
      if (response.data.status_code === 200) {
        window.alert(response.data.message);
        window.location = "/admin"
      }else {
        window.alert(response.data.message);
      } 
    })
  }


  function handleClickDetails(){
    setOpen(true)
    axios.get(`http://127.0.0.1:8000/tracker/website/${props.recordForEdit}`).then((response)=>{
      console.log("response-website:",response);
      if(response.data.status_code===200){
        setwebsitename(response.data.results);
      }
    })
    if(props.recordForEdit != null) {
      axios.get(`http://127.0.0.1:8000/tracker/website/${props.recordForEdit}`).then((response) => {
        if(response.data.status_code === 200) {
          console.log('website', response.data);
          setwebsite(response.data.result.websitename);
          console.log("website12",response.data.result.websitename)

        }
      })
      console.log("edit",recordForEdit)
    }
    else {
      setwebsite('');
     
    }
  
  }

  return (
    <div>
      <IconButton variant="outlined" color="primary" onClick={handleClickDetails}>
      <EditOutlinedIcon/>

      </IconButton>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
         <strong>website</strong>
        </DialogTitle>
        <DialogContent dividers>
          
  <form noValidate autoComplete="off" >
       
        <Grid item xs={12} md={12} lg={12} >
        <Grid item xs={12} md={12} lg={12}>
          <Grid conatiner item xs={12} md={12} lg={12} className="label">
                    <TextField  
                         id="input-with-icon-grid"
                          label="Website"
                          variant = 'outlined'
                          value = {website}
                          onChange = {(e)=>setwebsite(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                              </InputAdornment>
                            ),
                          }}
                          />
                          </Grid>
                          </Grid>
                          </Grid>
                          <Grid className="label">
                          <Button  variant="contained" color="primary" style={{marginTop:"15px" ,marginLeft:"30%"}}
                          onClick = {handleSubmit}
                          >Update</Button>

                          </Grid>
  
    </form>
        </DialogContent>
        <DialogActions>
        
        </DialogActions>
      </Dialog>
    </div>
  );
}























