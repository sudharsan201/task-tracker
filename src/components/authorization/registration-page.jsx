import React,{useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button,IconButton } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Logo from '../../images/TaskTrackerLogo.png';
import InputAdornment from '@material-ui/core/InputAdornment';
import Bg from '../../images/TastTrackerBack.png';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import axios from 'axios';
import { message} from 'antd';
import './style.css';
import 'antd/dist/antd.css';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  logo:{
    marginTop: '6%',
  },
  head:{
    width: 'unset !important',
    margin: '0 !important',
    backgroundImage:`url(${Bg})`,
    backgroundRepeat:'no-repeat',
    position:'relative',
    backgroundPosition:'center',
  },
  name:{
    
    marginBottom: '12px',
    marginTop: '15px',
    
  },
  label:{
    width: '196% !important',
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width:'auto',
    height:'auto',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',

  },
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function RegistrationPage() {
  const classes = useStyles();
  const history = useHistory();
  const [firstName, setFirstName]=useState('');
  const [lastName, setLastName]=useState('');
  const [email, setEmail]=useState('');
  const [userName, setUserName]=useState('');
  const [password, setPassword]=useState('');
  const [confirmPassword, setConfirmPassword]=useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [showconfirmPassword,setShowconfirmPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);
  const handleClickShowConfirmPassword = ()=> setShowconfirmPassword(!showconfirmPassword);
  const handleMouseupPassword = () => setShowconfirmPassword(!showconfirmPassword);


  function handleLogin(){
    history.push('/');
  }

  async function handleRegister(){
    const data = {
      first_name:firstName,
      last_name:lastName,
      email:email,
      username:userName,
      password:password,
      password2:confirmPassword,
    }
    try{
      await axios.post(`http://127.0.0.1:8000/tracker/register/`,data).then((response)=>{
        console.log("response:post",response)
        if(response.data.status_code === 201){
          message.success(response.data.message);
          setFirstName('');
          setPassword('');
          setLastName('');
          setUserName('');
          setConfirmPassword('');
          setEmail('');
          history.push('/');
        }else{
          message.error(response.data.message);
        }
      })
    }catch(error){
      message.error(error.message)
    }
    
  }





  return (
    <div className={classes.root}>
      <Grid container spacing={2} className={classes.head}>
        <Grid item xs={12} md={12} lg={12}>
        </Grid>
        <Grid item xs={12} md={8} lg={6} className={classes.logo  }>
        <img src={Logo} alt="logo"/>
        </Grid>
        <Grid item xs={12} md={4} lg={6}>
           <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                    <Grid item xs={12} md={12} lg={12} className={classes.name}>
                       <Typography variant="h4">Registration</Typography>
                    </Grid>
                    </Grid>
                </Grid>
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                    
                     <Grid item xs={10} md={10} lg={6} >
                        <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="First Name"
                          variant = 'outlined'
                          value = {firstName}
                          onChange={(e)=>setFirstName(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          />
                    </Grid>
                    </Grid>
                </Grid>
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                      <Grid item xs={10} md={10} lg={6} >
                        <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="Last Name"
                          value={lastName}
                          onChange={(e)=>setLastName(e.target.value)}
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          />
                    </Grid>
                    </Grid>
                </Grid>
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                      <Grid item xs={10} md={10} lg={6} >
                        <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="Email"
                          value={email}
                          onChange={(e)=>setEmail(e.target.value)}
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          />
                    </Grid>
                    </Grid>
                </Grid>
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                   
                    <Grid item xs={10} md={10} lg={6} >
                        <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="Username"
                          value={userName}
                          onChange={(e)=>setUserName(e.target.value)}
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          />
                    </Grid>
                    </Grid>
                </Grid>
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                    <Grid item xs={10} md={10} lg={6} >

                    <TextField className={classes.label}
                        label='Password'
                        variant="outlined"
                        type={showPassword ? "text" : "password"} 
                        onChange = {(e)=>setPassword(e.target.value)}
                        InputProps={{ 
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                              >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                      />
                        {/* <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="Password"
                          value={password}
                          onChange={(e)=>setPassword(e.target.value)}
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          /> */}
                    </Grid>
                    </Grid>
                </Grid>
                
               </Grid>
               <Grid item xs={12} md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container spacing={1} alignItems="flex-end">
                    <Grid item xs={10} md={10} lg={6} >
                    <TextField className={classes.label}  
                        label='Confirm Password'
                        variant="outlined"
                        type={showconfirmPassword ? "text" : "password"} 
                        onChange = {(e)=>setConfirmPassword(e.target.value)}
                        InputProps={{ 
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowConfirmPassword}
                                onMouseDown={handleMouseupPassword}
                              >
                                {showconfirmPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                      />
                        {/* <TextField className={classes.label}
                        required
                         id="input-with-icon-grid"
                          label="Confirm Password"
                          value={confirmPassword}
                          onChange={(e)=>setConfirmPassword(e.target.value)}
                          variant = 'outlined'
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          /> */}
                    </Grid>
                    </Grid>
                </Grid>
                
               </Grid>
               <Grid item xs={12}  md={12} lg={6}>
               <Grid className={classes.margin}>
                    <Grid container alignItems="flex-end" className={classes.name}> 
                    <Grid item xs={10} md={10} lg={12}>
                      <Button
                        variant="contained" 
                        color="primary"
                        onClick={handleRegister}
                        style={{color:"#fff",fontWeight:"bold"}}

                        >
                          Register
                      </Button>
                    </Grid>
                    <Grid item xs={10} md={10} lg={12} >
                     <Typography>Already Have an Account?<Button onClick={handleLogin}
                      style={{color:"#fff",fontWeight:"bold"}}
                      >
                        Sign in 
                      </Button>
                      </Typography>
                    </Grid>
                    </Grid>
                </Grid>
               </Grid> 

        </Grid>

      </Grid>
    </div>
  );
}