import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Logo from '../../images/TaskTrackerLogo.png';
import InputAdornment from '@material-ui/core/InputAdornment';
import Bg from '../../images/TastTrackerBack.png';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import axios from 'axios';
import { message} from 'antd';
import './style.css';
import 'antd/dist/antd.css';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
   
  },
  main:{
    display: 'flex',
    alignItems:'center',
    justifyContent:'center',
  },
  image:{
    
   
  },
  label:{
    width: '140% !important',
  },
  logo:{
    marginTop: '6%',

  },
  head:{
    backgroundImage:`url(${Bg})`,
    backgroundRepeat:'no-repeat',
    position:'relative',
    backgroundPosition:'center',
    width: 'unset !important',
    margin: '0 !important',
  },
  name:{
    marginBottom: '12px',
    marginTop: '15px',


  },
 

  margin: {
    margin: theme.spacing(1),
  },
}));

export default function LoginPage() {
  const history = useHistory();
  const classes = useStyles();
  const [alert, setAlert] = useState(false);
  const [data, setData] = useState();
  const [username,setUsername] = useState();
  const [password,setPassword] = useState();
  const [login, setLogin] = useState();

  const [showPassword, setShowPassword] = useState(false);
const handleClickShowPassword = () => setShowPassword(!showPassword);
const handleMouseDownPassword = () => setShowPassword(!showPassword);




  function handleRegister(){
    history.push('/register');
  }
  function handleLoginRequest(){
    if(!username){
      message.warning('Enter Username');
    }
    else if(!password){
      message.warning('Enter Password');
    }
    if(username && password){
      const data = {
        username:username,
        password:password,
      }
      console.log('machingdata',data)
  
    axios.post(`http://127.0.0.1:8000/tracker/login/`,data).then((response)=>{
      console.log("response:post",response)
      if(response.data.status_code === 200){
         roleCheck(response.data.data.id);
          localStorage.setItem('User', response.data.data.username);
         setUsername('');
         setPassword('');
      }else{
        message.error(response.data.message);
      }
      console.log('data',data)
    })
    }
    
  }


  

  async function roleCheck(id){
    try
    {
      await axios.get(`http://127.0.0.1:8000/tracker/role-access/?user_id=${id}`).then((response)=>{
        console.log('check-response:',response)
        if(response.data.status_code===200){
          console.log('response:',response.data)
          if(response.data.role){
            if(response.data.role==='Admin'){
              history.push('/admin', 1000);
              localStorage.setItem('Role', response.data.role)
            }
            else{
              history.push('/user' , 1000);
              localStorage.setItem('Role', response.data.role)
            }
          }
          else{
            history.push('/unauthorized')
          }
          
        }
      }
      );
    }catch(error){
      console.log('check-access',error)
      history.push('/unauthorized');
    }
    
  }

  useEffect(()=>{
    axios.get(`http://127.0.0.1:8000/tracker/login/`).then((response)=>{
      console.log("response-user:",response);
      if(response.data.status_code===200){
        setLogin(response.data.results);
      }
    })
    console.log("login:",data)
  },[login])



  return (
    <div className={classes.root}>
      <Grid spacing={2} className={classes.head}>
          <Grid container item xs={12} md={12} lg={12}>
          </Grid>
        <Grid item xs={12} md={12} lg={12} className={classes.main}>
          <Grid item xs={12} md={6} lg={6} className={classes.logo}>
           <img src={Logo} alt="logo"/>
          </Grid>
        <Grid item xs={12} md={6} lg={6} className={classes.margin}>
           <Grid item xs={6} md={6} lg={6} className={classes.name}>
                <Typography variant="h4">Login</Typography>
            </Grid>
               <Grid item xs={10} md={10} lg={4} className={classes.name}>
               <TextField className={classes.label}
                         id="input-with-icon-grid"
                          label="Username"
                          variant = 'outlined'
                          onChange={(e)=>setUsername(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          />
               </Grid>
                    <Grid item xs={10} md={10} lg={4} className={classes.name}>
                    <TextField className={classes.label}
                        id="input-with-icon-grid"
                        label='Password'
                        variant="outlined"
                        type={showPassword ? "text" : "password"} 
                        onChange = {(e)=>setPassword(e.target.value)}
                        InputProps={{ 
                          endAdornment: (
                            <InputAdornment position="start">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                              >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                      />
                        {/* <TextField className={classes.label}
                          required = {true}
                         id="input-with-icon-grid"
                          label="Password"
                          variant = 'outlined'
                          onChange = {(e)=>setPassword(e.target.value)}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountCircle />
                              </InputAdornment>
                            ),
                          }}
                          /> */}
               </Grid>
                    <Grid item xs={10} md={10} lg={6} className={classes.name}>
                      <Button
                        variant="contained"
                         color="primary"
                          size="large"
                          onClick = {handleLoginRequest}
                          style={{color:"#fff",fontWeight:"bold"}}
                          >
                            Login
                            </Button>

                    </Grid>
                    <Grid item xs={10} md={10} lg={6} className={classes.name}>
                      <Button
                        variant="contained"
                         color="primary"
                          size="large"
                          onClick={handleRegister}
                          style={{color:"#fff",fontWeight:"bold"}}
                          >
                            Create New Account
                            </Button>
                    </Grid>
                  </Grid>
                  </Grid>
                  </Grid>
     
    </div>
  );
}