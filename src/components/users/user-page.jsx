import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton, Select } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import Bg from '../../images/TastTrackerBack.png';
import users from '../../config/services';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import { useHistory } from 'react-router-dom';
import UserForm from './user-update';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';



const useStyles = makeStyles((theme) => ({
  root: {

    display: 'inlineFlex:',
    backgroundImage: `url(${Bg})`,
    backgroundRepeat: 'no-repeat',
    position: 'relative',
    backgroundPosition: "unset",
    backgroundPositionX: "center",
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },

  },
  logo: {
    marginTop: '6%',
  },
  TableHead: {
    backgroundColor: "darkgray",


  },
  name: {

    marginBottom: '12px',
    marginTop: '15px',
  },
  label: {
    width: '100%',

  },
  paper: {

    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: 'auto',
    height: 'auto',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',

  },
  margin: {
    margin: theme.spacing(1),
  },
  formControl: {
    width: '100%',
    marginTop: '3%',
  }

}));

export default function Users(props) {

  const history = useHistory();
  const classes = useStyles();
  const [userLists, setUserLists] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [count, setCount] = useState();
  const [open, setOpen] = React.useState(false);
  const [erpid, setErpId] = useState('');
  const [mobile, setMobile] = useState('');
  const [roletype, setRoleType] = useState([]);
  const [Usertype, setUserType] = useState([]);
  const [userS, setUsers] = useState([]);
  const [roles, SetRole] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [refresh,setRefresh]=useState(false)
  const sNo = 1;







  const handleClickDetails = () => {
    setOpen(true);
  };


  const handleChangePage = (event, newPage) => {
    console.log(newPage)
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(1);
  };
  const [state, setState] = React.useState({});

  const handleChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  function handleUpdate() {
    history.push('/user-update');
    setOpen(true);
  }
  const handleClose = () => {
    setOpen(false);
  };

  // useEffect(() => {
    
  // },[refresh]);

  function handleSubmit() {
    if (!erpid) {
      message.warning('Please Enter Erp Id');
    }
    else if (!mobile) {
      message.warning('Please Enter Mobile NUmber');
    }
    else if (!Usertype) {
      message.warning('Please Select User');
    }
    else if (!roletype) {
      console.log('roletype', roletype)
      message.warning('Please Select Role');
    }
    else if (erpid && mobile && Usertype && roletype) {
      const data = {
        erp_id: parseInt(erpid),
        mobile_number: parseInt(mobile),
        employee_name_id: parseInt(Usertype),
        role_type_id: parseInt(roletype),

      }
      console.log(data);
      axios.post(`http://127.0.0.1:8000/tracker/employee/`, data).then((response) => {
        console.log("response:post", response)
        setRefresh(!refresh)
        if (response.data.status_code === 201) {
          message.success(response.data.message);
          setErpId('');
          setMobile('');
          setUserType('');
          setRoleType('');

        } else {
          message.error(response.data.message);
        }
      })
    }

  }




  function handleDeleted(data) {
    axios.delete(`http://127.0.0.1:8000/tracker/employee/${data.id}`).then((response) => {
      setRefresh(!refresh)
      if (response.status_code === 404) {
        message.error(response.data.message);
        history.push('/Users');
      } else {
        message.success(response.data.message);
      }
    })
  }


  useEffect(() => {
    setPage(1)
    axios.get(`${users.users.userList}?page_size=${rowsPerPage}&page=${page}`).then((response) => {
      console.log(response.data)
      // setRefresh(!refresh)
      if (response.data.status_code === 200) {
        setUserLists(response.data.results);
        setCount(response.data.count);
      } else {
        console.log('alert', response.message)
      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/user-list/`).then((response) => {
      console.log("response-user:", response);
      if (response.data.status_code === 200) {
        setUsers(response.data.userList);

      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/role/`).then((response) => {
      console.log("response-role:", response);
      if (response.data.status_code === 200) {
        SetRole(response.data.results)
      }
    })
  }, [page,refresh])




  return (
    <div className={classes.root}>
      <form className={classes.root}>
        <Typography variant="h4">User</Typography>
        <Grid container item xs={12} md={12} lg={12} spacing={4}>
          <Grid item xs={2} md={2} lg={2}>
            <TextField className={classes.label}
              id="input-with-icon-grid"
              label="ERP ID"
              variant='outlined'
              value={erpid}
              onChange={(e) => setErpId(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <TextField className={classes.label}
              id="input-with-icon-grid"
              label="Mobile Number"
              variant='outlined'
              value={mobile}
              onChange={(e) => setMobile(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                  </InputAdornment>
                ),
              }}

            />
          </Grid>
          <Grid item xs={2} md={2} lg={2} >
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel htmlFor="outlined-age-native-simple">User Name</InputLabel>
              <Select
                native
                value={Usertype}
                onChange={(e) => setUserType(e.target.value)}
                label="UserName"
                inputProps={{
                  name: 'username',
                  id: 'outlined-age-native-simple',
                }}
              >
                <option aria-label="None" value="" />
                {userS && userS.map((user) =>
                <option value={user.id}>{user.username}</option>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel htmlFor="outlined-age-native-simple">Role</InputLabel>
              <Select
                native
                value={roletype}
                onChange={(e) => setRoleType(e.target.value)}
                label="Role"
                inputProps={{
                  name: 'Role',
                  id: 'outlined-age-native-simple',
                }}
              >
                {roles && roles.map((rolee) =>
                  <option value={rolee.id}>{rolee.role}</option>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Button
              variant="contained"
              color="primary"
              style={{ color: "#fff", fontWeight: "bold", marginTop: "5%" }}
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </form>
      <Grid container xs={12} md={12} lg={8} spacing={3}>
        <Grid item xs={4} md={4} lg={4}>
          <TextField className={classes.label}
            id="input-with-icon-grid"
            label="Search"
            variant='outlined'
            onChange={(e) => setSearchValue(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper style={{ backgroundColor: "unset" }}>
          <TableContainer >
            <Table>
              <TableHead >
                <TableRow style={{ backgroundColor: "darkgray", color: "#fff", fontWeight: "bold" }}>
                  <TableCell> S.No</TableCell>
                  <TableCell> Erp Id</TableCell>
                  <TableCell> User Name</TableCell>
                  <TableCell> Mobile Number</TableCell>
                  <TableCell> Role </TableCell>
                  <TableCell> Email</TableCell>
                  <TableCell> Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userLists.map((item, index ) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={1} key={item.id}>
                      <TableCell>{index+sNo}</TableCell>
                      <TableCell>{item.erp_id}</TableCell>
                      <TableCell>{item.employee_name?.username}</TableCell>
                      <TableCell>{item.mobile_number}</TableCell>
                      <TableCell>{item.role_type?.role}</TableCell>
                      <TableCell>{item.employee_name?.email}</TableCell>
                      <TableCell>
                        <IconButton
                          color='primary'
                          onClick={() => { handleClickDetails(item) }}
                        >
                          <UserForm
                            recordForEdit={item.id}
                          />
                        </IconButton>
                        <IconButton
                          color='primary'
                          onClick={() => { handleDeleted(item) }}
                        >
                          <DeleteForeverOutlinedIcon />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={count}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </Grid>
    </div>
  );
}



















