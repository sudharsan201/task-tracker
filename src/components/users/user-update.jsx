import Styles from './style.css';
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { TextField, Select } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';



const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },



});
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundRepeat: 'no-repeat',
    position: 'relative',
    backgroundPosition: "unset",
    backgroundPositionX: "center",
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: 300,
    },

  },
  logo: {
    marginTop: '6%',
  },
  TableHead: {
    top: 0,
    left: 0,
    zIndex: 2,
    Position: 'sticky',
    BackgroundColor: 'black',


  },
  name: {

    marginBottom: '12px',
    marginTop: '15px',
  },
  label: {
    marginBottom: '12px',
  },
  paper: {

    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: 'auto',
    height: 'auto',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',

  },
  margin: {
    margin: theme.spacing(1),
  },
  formControl: {
    width: '30Vh',
    marginBottom: '12px',
    textAlign: 'center',
    marginTop:'10px',

  },

}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function UserForm(props) {

  const { addorEdit, recordForEdit } = props
  console.log("record",props.recordForEdit)
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const [erpid, setErpId] = useState('');
  const [mobile, setMobile] = useState('');
  const [roletype, setRoleType] = useState('');
  const [Usertype, setUserType] = useState([]);
  const [userS, setUsers]=useState([]);
  const [roles, SetRole] = useState([]);
  const [email, setEmail] = useState('');
  const [refresh, setRefresh] = useState(false)




  const handleClose = () => {
    setOpen(false);
  };



  function handleUpdate(){
    const data = {
        erp_id:erpid ,
        mobile_number:mobile,
        employee_name_id:parseInt(Usertype),
        role_type_id:parseInt(roletype),
        
    }
    console.log("data",data);
    axios.put(`http://127.0.0.1:8000/tracker/employee/${props.recordForEdit}`, data).then((response) => {
      setRefresh(!refresh)
      if (response.data.status_code === 200) {
        message.success(response.data.message);
        handleClose()
      }else {
        message.error(response.data.message);
      } 
    })
    console.log("userupdate", props.recordForEdit)
  }


  function handleClickDetails(){
    setOpen(true) 
    axios.get(`http://127.0.0.1:8000/tracker/role/`).then((response)=>{
      console.log("response-role:",response);
      // setRefresh(!refresh)
      if(response.data.status_code ===200){
        SetRole(response.data.results)
      }
    })
    axios.get(`http://127.0.0.1:8000/tracker/user-list/`).then((response) => {
      console.log("response-user:", response);
      if (response.data.status_code === 200) {
        setUsers(response.data.userList);

      }
    })
    if(props.recordForEdit != null) {
      axios.get(`http://127.0.0.1:8000/tracker/employee/${props.recordForEdit}`).then((response) => {
        if(response.data.status_code === 200) {
          console.log('user', response.data);
          setErpId(response.data.result.erp_id);

          setMobile(response.data.result.mobile_number);

          setUserType(response.data.result.employee_name.id);

          setEmail(response.data.result.employee_name.email);

          setRoleType(response.data.result.role_type.id);

        }
      })
    }
    else {
      setErpId('');
      setMobile('');
      setUserType('');
      setRoleType('');  
    }
  }

 

  return (
    <div>
      <IconButton variant="outlined" color="primary" onClick={handleClickDetails}>
      <EditOutlinedIcon />
      </IconButton>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <strong>User</strong>
        </DialogTitle>
        <DialogContent dividers>
          <form  noValidate autoComplete="off" >
            <Grid item xs={12} md={12} lg={12} >
              <Grid item xs={12} md={12} lg={12}>
                <Grid conatiner item xs={12} md={12} lg={12} className="label">
                  <TextField
                    id="input-with-icon-grid"
                    label="ERP ID"
                    variant='outlined'
                    value={erpid}
                    onChange={(e) => setErpId(e.target.value)}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                <Grid className="label">
                  <TextField
                    id="input-with-icon-grid"
                    label=" Email"
                    variant='outlined'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>

                <Grid className="label">
                  <TextField
                    id="input-with-icon-grid"
                    label="Mobile Number"
                    variant='outlined'
                    value={mobile}
                    onChange={(e) => setMobile(e.target.value)}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid container xs={12} md={12} lg={6}>
              <Grid item xs={12} md={12} lg={6} >
                <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-age-native-simple">UserName</InputLabel>
                  <Select
                    native
                    value={Usertype}
                    // disabled
                    onChange={(e) => setUserType(e.target.value)}
                    label="UserName"
                    inputProps={{
                      name: 'username',
                      id: 'outlined-age-native-simple',
                    }}
                  > 
                       {userS && userS.map((user)=>
                      <option value={user.id}>{user.username}</option>
                      )}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid container xs={12} md={12} lg={6}>
              <Grid item xs={12} md={12} lg={6}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel htmlFor="outlined-age-native-simple">RoleType</InputLabel>
                  <Select
                    native
                    value={roletype}
                    onChange={(e) => setRoleType(e.target.value)}
                    label="RoleType"
                    inputProps={{
                      name: 'roletype',
                      id: 'outlined-age-native-simple',
                    }}
                  >
                    {roles && roles.map((rolee)=>
                    <option value={rolee.id}>{rolee.role}</option>)}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid className="label">
            <Button variant="contained" color="primary" style={{ marginTop: "15px", marginLeft: "30%", color:'#fff',fontWeight:"bold" }}
              onClick = {handleUpdate}
              >Update</Button>
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
        </DialogActions>
      </Dialog>
    </div>
  );
}























