let BASE = '';
if (typeof window !== 'undefined') {
  const host = window.location.hostname;
  console.log('in',host)
  if (host === 'localhost') {
    BASE = 'http://localhost:8000/tracker';
  }
  else {
    BASE = 'http://localhost:8000/tracker';
   
  }

}

export default {
  users: {
    userList: `${BASE}/employee/`,
  },
  tasks: {
    taskList: `${BASE}/task/`,
  },
  website: {
    websiteList: `${BASE}/website/`,
  },
  role: {
    roleList: `${BASE}/role/`,
  },
  login:{
    loginList: `${BASE}/login/`,
  },
  registration:{
    registerList: `${BASE}/register/`,
  },
};